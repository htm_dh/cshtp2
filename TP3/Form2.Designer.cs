﻿namespace TP3
{
    partial class Form2
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Form2));
            this.contextMenuStrip1 = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.listeToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.déplacementDeDonnéesToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.exitToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.parametresToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.afficherBarreDoutilToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.desactiverBarreDoutilToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.afficheImageToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.changerCouleurToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.aideToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.aProposToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStrip1 = new System.Windows.Forms.ToolStrip();
            this.toolStripButton1 = new System.Windows.Forms.ToolStripButton();
            this.toolStripButton2 = new System.Windows.Forms.ToolStripButton();
            this.toolStripButton3 = new System.Windows.Forms.ToolStripButton();
            this.contextMenuStrip2 = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.déplacementDesDonnéesToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.afficherLaBareDoutilsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.afficheLimageSurLeBureauToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.aideToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.àProposToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.exitToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.menuStrip1.SuspendLayout();
            this.toolStrip1.SuspendLayout();
            this.contextMenuStrip2.SuspendLayout();
            this.SuspendLayout();
            // 
            // contextMenuStrip1
            // 
            this.contextMenuStrip1.ImageScalingSize = new System.Drawing.Size(20, 20);
            this.contextMenuStrip1.Name = "contextMenuStrip1";
            this.contextMenuStrip1.Size = new System.Drawing.Size(61, 4);
            this.contextMenuStrip1.Opening += new System.ComponentModel.CancelEventHandler(this.contextMenuStrip1_Opening);
            // 
            // menuStrip1
            // 
            this.menuStrip1.ImageScalingSize = new System.Drawing.Size(20, 20);
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.listeToolStripMenuItem,
            this.parametresToolStripMenuItem,
            this.toolStripMenuItem1});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(800, 28);
            this.menuStrip1.TabIndex = 4;
            this.menuStrip1.Text = "menuStrip1";
            this.menuStrip1.ItemClicked += new System.Windows.Forms.ToolStripItemClickedEventHandler(this.menuStrip1_ItemClicked);
            // 
            // listeToolStripMenuItem
            // 
            this.listeToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.déplacementDeDonnéesToolStripMenuItem,
            this.exitToolStripMenuItem});
            this.listeToolStripMenuItem.Name = "listeToolStripMenuItem";
            this.listeToolStripMenuItem.Size = new System.Drawing.Size(53, 24);
            this.listeToolStripMenuItem.Text = "Liste";
            // 
            // déplacementDeDonnéesToolStripMenuItem
            // 
            this.déplacementDeDonnéesToolStripMenuItem.Image = ((System.Drawing.Image)(resources.GetObject("déplacementDeDonnéesToolStripMenuItem.Image")));
            this.déplacementDeDonnéesToolStripMenuItem.Name = "déplacementDeDonnéesToolStripMenuItem";
            this.déplacementDeDonnéesToolStripMenuItem.Size = new System.Drawing.Size(266, 26);
            this.déplacementDeDonnéesToolStripMenuItem.Text = "Déplacement de données ";
            this.déplacementDeDonnéesToolStripMenuItem.Click += new System.EventHandler(this.déplacementDeDonnéesToolStripMenuItem_Click);
            // 
            // exitToolStripMenuItem
            // 
            this.exitToolStripMenuItem.Image = ((System.Drawing.Image)(resources.GetObject("exitToolStripMenuItem.Image")));
            this.exitToolStripMenuItem.Name = "exitToolStripMenuItem";
            this.exitToolStripMenuItem.Size = new System.Drawing.Size(266, 26);
            this.exitToolStripMenuItem.Text = "Exit";
            // 
            // parametresToolStripMenuItem
            // 
            this.parametresToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.afficherBarreDoutilToolStripMenuItem,
            this.desactiverBarreDoutilToolStripMenuItem,
            this.afficheImageToolStripMenuItem,
            this.changerCouleurToolStripMenuItem});
            this.parametresToolStripMenuItem.Name = "parametresToolStripMenuItem";
            this.parametresToolStripMenuItem.Size = new System.Drawing.Size(96, 24);
            this.parametresToolStripMenuItem.Text = "Parametres";
            // 
            // afficherBarreDoutilToolStripMenuItem
            // 
            this.afficherBarreDoutilToolStripMenuItem.Image = ((System.Drawing.Image)(resources.GetObject("afficherBarreDoutilToolStripMenuItem.Image")));
            this.afficherBarreDoutilToolStripMenuItem.Name = "afficherBarreDoutilToolStripMenuItem";
            this.afficherBarreDoutilToolStripMenuItem.Size = new System.Drawing.Size(246, 26);
            this.afficherBarreDoutilToolStripMenuItem.Text = "Afficher Barre d\'outil";
            // 
            // desactiverBarreDoutilToolStripMenuItem
            // 
            this.desactiverBarreDoutilToolStripMenuItem.Image = ((System.Drawing.Image)(resources.GetObject("desactiverBarreDoutilToolStripMenuItem.Image")));
            this.desactiverBarreDoutilToolStripMenuItem.Name = "desactiverBarreDoutilToolStripMenuItem";
            this.desactiverBarreDoutilToolStripMenuItem.Size = new System.Drawing.Size(246, 26);
            this.desactiverBarreDoutilToolStripMenuItem.Text = "Desactiver Barre d\'outil";
            // 
            // afficheImageToolStripMenuItem
            // 
            this.afficheImageToolStripMenuItem.Image = ((System.Drawing.Image)(resources.GetObject("afficheImageToolStripMenuItem.Image")));
            this.afficheImageToolStripMenuItem.Name = "afficheImageToolStripMenuItem";
            this.afficheImageToolStripMenuItem.Size = new System.Drawing.Size(246, 26);
            this.afficheImageToolStripMenuItem.Text = "Affiche Image";
            // 
            // changerCouleurToolStripMenuItem
            // 
            this.changerCouleurToolStripMenuItem.Image = ((System.Drawing.Image)(resources.GetObject("changerCouleurToolStripMenuItem.Image")));
            this.changerCouleurToolStripMenuItem.Name = "changerCouleurToolStripMenuItem";
            this.changerCouleurToolStripMenuItem.Size = new System.Drawing.Size(246, 26);
            this.changerCouleurToolStripMenuItem.Text = "Changer Couleur";
            // 
            // toolStripMenuItem1
            // 
            this.toolStripMenuItem1.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.aideToolStripMenuItem,
            this.aProposToolStripMenuItem});
            this.toolStripMenuItem1.Name = "toolStripMenuItem1";
            this.toolStripMenuItem1.Size = new System.Drawing.Size(30, 24);
            this.toolStripMenuItem1.Text = "?";
            // 
            // aideToolStripMenuItem
            // 
            this.aideToolStripMenuItem.Name = "aideToolStripMenuItem";
            this.aideToolStripMenuItem.Size = new System.Drawing.Size(224, 26);
            this.aideToolStripMenuItem.Text = "Aide";
            // 
            // aProposToolStripMenuItem
            // 
            this.aProposToolStripMenuItem.Name = "aProposToolStripMenuItem";
            this.aProposToolStripMenuItem.Size = new System.Drawing.Size(224, 26);
            this.aProposToolStripMenuItem.Text = "A propos";
            // 
            // toolStrip1
            // 
            this.toolStrip1.ImageScalingSize = new System.Drawing.Size(20, 20);
            this.toolStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripButton1,
            this.toolStripButton2,
            this.toolStripButton3});
            this.toolStrip1.Location = new System.Drawing.Point(0, 28);
            this.toolStrip1.Name = "toolStrip1";
            this.toolStrip1.Size = new System.Drawing.Size(800, 27);
            this.toolStrip1.TabIndex = 5;
            this.toolStrip1.Text = "toolStrip1";
            // 
            // toolStripButton1
            // 
            this.toolStripButton1.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.toolStripButton1.Image = ((System.Drawing.Image)(resources.GetObject("toolStripButton1.Image")));
            this.toolStripButton1.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButton1.Name = "toolStripButton1";
            this.toolStripButton1.Size = new System.Drawing.Size(29, 24);
            this.toolStripButton1.Text = "toolStripButton1";
            this.toolStripButton1.Click += new System.EventHandler(this.toolStripButton1_Click);
            // 
            // toolStripButton2
            // 
            this.toolStripButton2.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.toolStripButton2.Image = ((System.Drawing.Image)(resources.GetObject("toolStripButton2.Image")));
            this.toolStripButton2.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButton2.Name = "toolStripButton2";
            this.toolStripButton2.Size = new System.Drawing.Size(29, 24);
            this.toolStripButton2.Text = "toolStripButton2";
            // 
            // toolStripButton3
            // 
            this.toolStripButton3.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.toolStripButton3.Image = ((System.Drawing.Image)(resources.GetObject("toolStripButton3.Image")));
            this.toolStripButton3.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButton3.Name = "toolStripButton3";
            this.toolStripButton3.Size = new System.Drawing.Size(29, 24);
            this.toolStripButton3.Text = "toolStripButton3";
            // 
            // contextMenuStrip2
            // 
            this.contextMenuStrip2.ImageScalingSize = new System.Drawing.Size(20, 20);
            this.contextMenuStrip2.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.déplacementDesDonnéesToolStripMenuItem,
            this.afficherLaBareDoutilsToolStripMenuItem,
            this.afficheLimageSurLeBureauToolStripMenuItem,
            this.aideToolStripMenuItem1,
            this.àProposToolStripMenuItem,
            this.exitToolStripMenuItem1});
            this.contextMenuStrip2.Name = "contextMenuStrip2";
            this.contextMenuStrip2.Size = new System.Drawing.Size(270, 148);
            // 
            // déplacementDesDonnéesToolStripMenuItem
            // 
            this.déplacementDesDonnéesToolStripMenuItem.Name = "déplacementDesDonnéesToolStripMenuItem";
            this.déplacementDesDonnéesToolStripMenuItem.Size = new System.Drawing.Size(269, 24);
            this.déplacementDesDonnéesToolStripMenuItem.Text = "Déplacement des données";
            // 
            // afficherLaBareDoutilsToolStripMenuItem
            // 
            this.afficherLaBareDoutilsToolStripMenuItem.Name = "afficherLaBareDoutilsToolStripMenuItem";
            this.afficherLaBareDoutilsToolStripMenuItem.Size = new System.Drawing.Size(269, 24);
            this.afficherLaBareDoutilsToolStripMenuItem.Text = "Afficher la bare d\'outils";
            // 
            // afficheLimageSurLeBureauToolStripMenuItem
            // 
            this.afficheLimageSurLeBureauToolStripMenuItem.Name = "afficheLimageSurLeBureauToolStripMenuItem";
            this.afficheLimageSurLeBureauToolStripMenuItem.Size = new System.Drawing.Size(269, 24);
            this.afficheLimageSurLeBureauToolStripMenuItem.Text = "Affiche l\'image Sur le bureau";
            // 
            // aideToolStripMenuItem1
            // 
            this.aideToolStripMenuItem1.Name = "aideToolStripMenuItem1";
            this.aideToolStripMenuItem1.Size = new System.Drawing.Size(269, 24);
            this.aideToolStripMenuItem1.Text = "Aide";
            // 
            // àProposToolStripMenuItem
            // 
            this.àProposToolStripMenuItem.Name = "àProposToolStripMenuItem";
            this.àProposToolStripMenuItem.Size = new System.Drawing.Size(269, 24);
            this.àProposToolStripMenuItem.Text = "à propos";
            // 
            // exitToolStripMenuItem1
            // 
            this.exitToolStripMenuItem1.Name = "exitToolStripMenuItem1";
            this.exitToolStripMenuItem1.Size = new System.Drawing.Size(269, 24);
            this.exitToolStripMenuItem1.Text = "Exit";
            // 
            // Form2
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.ActiveBorder;
            this.ClientSize = new System.Drawing.Size(800, 450);
            this.ContextMenuStrip = this.contextMenuStrip2;
            this.Controls.Add(this.toolStrip1);
            this.Controls.Add(this.menuStrip1);
            this.Name = "Form2";
            this.Text = "Form2";
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            this.toolStrip1.ResumeLayout(false);
            this.toolStrip1.PerformLayout();
            this.contextMenuStrip2.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ContextMenuStrip contextMenuStrip1;
        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem listeToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem déplacementDeDonnéesToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem exitToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem parametresToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem afficherBarreDoutilToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem desactiverBarreDoutilToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem afficheImageToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem changerCouleurToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem aideToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem aProposToolStripMenuItem;
        private System.Windows.Forms.ToolStrip toolStrip1;
        private System.Windows.Forms.ToolStripButton toolStripButton1;
        private System.Windows.Forms.ToolStripButton toolStripButton2;
        private System.Windows.Forms.ToolStripButton toolStripButton3;
        private System.Windows.Forms.ContextMenuStrip contextMenuStrip2;
        private System.Windows.Forms.ToolStripMenuItem déplacementDesDonnéesToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem afficherLaBareDoutilsToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem afficheLimageSurLeBureauToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem aideToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem àProposToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem exitToolStripMenuItem1;
    }
}