﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace TP3
{
    public partial class Form1 : Form
    {

        public Form1()
        {
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            label16.Text = DateTime.Now.ToShortDateString();
            // LblHeure.Text = DateTime.Now.ToLongTimeString();
            timer1.Start();
            BtnAajout.Enabled = false;
        }

        private void groupBox1_Enter(object sender, EventArgs e)
        {

        }

        private void timer1_Tick(object sender, EventArgs e)
        {
            LblHeure.Text = DateTime.Now.ToLongTimeString();
        }

        private void lang_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (((e.KeyChar < '0') || (e.KeyChar > '9')))
                e.KeyChar = (char)0;
        }

        private void lang_Validating(object sender, CancelEventArgs e)
        {
            if (lang.Text == "")
            {
                MessageBox.Show("Saisir une note");
                e.Cancel = true;
            }
            else
            {
                if (int.Parse(lang.Text) > 20)
                {
                    MessageBox.Show("La note doit être entre 0 et 20");
                    lang.SelectAll();
                    e.Cancel = true;
                }
            }
            }

        private void tech_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (((e.KeyChar < '0') || (e.KeyChar > '9')))
                e.KeyChar = (char)0;
        }

        private void tech_Validating(object sender, CancelEventArgs e)
        {
            if (tech.Text == "")
            {
                MessageBox.Show("Il faut saisir une note");
                e.Cancel = true;

            }
            else
            {
                if (int.Parse(tech.Text) > 20)
                {
                    MessageBox.Show("La note doit être entre 0 et 20");
                    tech.SelectAll();
                    e.Cancel = true;
                }
            }
        }

        private void gen_Click(object sender, EventArgs e)
        {
            int x;
            Random alea = new Random();
            x = alea.Next(0, 16);
            TxtChance.Text = x.ToString();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            textBox1.Text = "";
            textBox2.Text = "";
            textBox3.Text = "";
            textBox4.Text = "";
            radioButton1.Checked = false;
            radioButton2.Checked = false;
            comboBox1.SelectedIndex = -1;
            BtnAajout.Enabled = false;



        }

        private void comboBox1_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        private void button3_Click(object sender, EventArgs e)
        {
            if ((textBox1.Text == "") || (textBox2.Text == "") || (textBox3.Text == "") || (textBox4.Text == ""))
 {
                MessageBox.Show("Il faut saisir toutes les informations");
                return;
            }
            int note, sexe, bonus =0, age=0, chance, score;
            note = (int.Parse(lang.Text)+int.Parse(tech.Text))/2;
            if (radioButton1.Checked == true)
            {
                sexe = 7;
            }
            else
                sexe = 5;
            if (checkBox1.Checked)
            {
                bonus = bonus + 15;
            }
            if (checkBox2.Checked)
            {
                bonus = bonus + 10;
            }
            if (checkBox3.Checked)
            {
                bonus = bonus + 5;
            }

            switch (comboBox1.SelectedIndex)
            {
                case 0:
                    age = 20; break;
                case 1:
                    age = 10;break;
                case 3:
                    age = 5;break;
            }
            chance = int.Parse(TxtChance.Text);
            score = note + sexe + bonus + age + chance;
            textBox8.Text = score.ToString();
            BtnAajout.Enabled = true;

        }
    }
    }
